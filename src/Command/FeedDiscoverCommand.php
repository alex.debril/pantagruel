<?php declare(strict_types = 1);

namespace App\Command;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Amqp\Connection as AmqpConnection;
use FeedIo\FeedIo;

class FeedDiscoverCommand extends Command
{

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \App\Amqp\Connection
     */
    private $amqpConnection;

    /**
     *
     * @var \FeedIo\FeedIo
     */
    private $feedIo;

    /**
     * @param Session $session
     */
    public function __construct(
        LoggerInterface $logger, 
        FeedIo $feedIo, 
        AmqpConnection $ampqConnection
        )
    {
        $this->logger = $logger;
        $this->feedIo = $feedIo;
        $this->amqpConnection = $ampqConnection;

        parent::__construct();
    }

    protected function configure()
    {
        $this
        ->setName('pantagruel:feed:discover')
        ->setDescription('Discover feeds from metadata')
        ->setHelp('This command discovers feeds in HTML pages');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $message = "Starting";

        $output->writeln($message);
        $this->logger->info($message);
        $feedIo = $this->feedIo;

        $urlsQueue = $this->amqpConnection->getQueue('urls');
        $feedDiscoverQueue = $this->amqpConnection->getQueue('feedDiscover');

        $feedDiscoverQueue->consume(function($message) use ($feedIo, $urlsQueue){
            try {
                $this->logger->info("discovering feeds from : {$message->body}");
                $urls = $feedIo->discover($message->body);
                foreach($urls as $url) {
                    $this->logger->info("discovered feed : {$url}");
                    $urlsQueue->enqueue($url);
                }
            } catch (\Exception $e) {
                $this->logger->warning("no feed found in {$message->body}");
        }

            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
        });

        $feedDiscoverQueue->wait();
    }

}
