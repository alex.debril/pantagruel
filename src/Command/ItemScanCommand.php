<?php declare(strict_types = 1);

namespace App\Command;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use App\Amqp\Connection as AmqpConnection;
use App\Collection\Item as ItemCollection;
use App\Entity\Item;

class ItemScanCommand extends Command
{

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \App\Amqp\Connection
     */
    private $amqpConnection;

    /**
     * @var \App\Collection\Item
     */
    private $itemCollection;

    /**
     *
     * @param LoggerInterface $logger
     * @param AmqpConnection $amqpConnection
     * @param Collection $collection
     */
    public function __construct(LoggerInterface $logger, AmqpConnection $amqpConnection, ItemCollection $itemCollection)
    {
        $this->logger = $logger;
        $this->amqpConnection = $amqpConnection;
        $this->itemCollection = $itemCollection;

        parent::__construct();
    }

    protected function configure()
    {
        $this
        ->setName('pantagruel:item:scan')
        ->setDescription('Scan url')
        ->setHelp('This command scans an item in search for new URLs');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $message = "Starting";
        $this->logger->info($message);

        $this->wait($output);
    }

    protected function wait(OutputInterface $output)
    {
        $this->logger->debug("scanning items for new urls");
        $items = $this->itemCollection->findRecordedAfter(new \DateTime('- 4hours'));
        foreach( $items as $item ) {
            $this->scan($item);
        }

        $this->logger->info('waiting');
        sleep(7200);
        $this->wait($output);
    }

    protected function scan(Item $item)
    {
        $urlQueue = $this->amqpConnection->getQueue('urls');

        try {
            $doc = new \DOMDocument();
            if ( is_null($item->getDescription()) ) {
                return true;
            }

            $doc->loadHTML($item->getDescription(), LIBXML_NOERROR);
            $xpath = new \DOMXpath($doc);
            $nodeList = $xpath->query('//a/@href');
            $urls = [];
            for ($i = 0; $i < $nodeList->length; $i++) {
                $url = $nodeList->item($i)->value;
                $urlComponents = parse_url($url);
                $host = "{$urlComponents['scheme']}://{$urlComponents['host']}";
                $urls[$host] = $i;
            }

            $urls = array_keys($urls);
            foreach($urls as $url) {
                if (filter_var($url, FILTER_VALIDATE_URL) ) {
                    $this->logger->info("found : {$url}");
                    $urlQueue->enqueue($url);
                }
            }
        } catch (\Exception $e) {
            $this->logger->warning('an issue occured while scanning the item : ' . $e->getMessage());
            $this->logger->warning($e->getTraceAsString());
        }
    }

}
