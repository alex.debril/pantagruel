<?php declare(strict_types = 1);

namespace App\Command;

use Psr\Log\LoggerInterface;
use App\Collection\Feed as FeedCollection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DataUpdateFeedCommand extends Command
{

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \App\Collection\Feed
     */
    private $feedCollection;

    /**
     * @param Session $session
     */
    public function __construct(
        LoggerInterface $logger,
        FeedCollection $feedCollection
        )
    {
        $this->logger = $logger;
        $this->feedCollection = $feedCollection;

        parent::__construct();
    }

    protected function configure()
    {
        $this
        ->setName('pantagruel:data:update')
        ->setDescription('Update feeds data')
        ->setHelp('This command updates data stored in the feed collection');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $message = "Starting";

        $feeds = $this->feedCollection->findAll();

        foreach($feeds as $feed) {
            $this->feedCollection->persist($feed);
        }
    }

}
