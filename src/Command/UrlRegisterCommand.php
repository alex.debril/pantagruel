<?php declare(strict_types = 1);

namespace App\Command;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use App\Amqp\Connection as AmqpConnection;

use App\Entity\Url;
use App\Collection\Url as Collection;

class UrlRegisterCommand extends Command
{

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \App\Amqp\Connection
     */
    private $amqpConnection;

    /**
     * @var \App\Collection\Url
     */
    private $collection;

    /**
     *
     * @param LoggerInterface $logger
     * @param AmqpConnection $amqpConnection
     * @param Collection $collection
     */
    public function __construct(LoggerInterface $logger, AmqpConnection $amqpConnection, Collection $collection)
    {
        $this->logger = $logger;
        $this->amqpConnection = $amqpConnection;
        $this->collection = $collection;

        parent::__construct();
    }

    protected function configure()
    {
        $this
        ->setName('pantagruel:url:register')
        ->setDescription('Register url')
        ->setHelp('This command stores url in database');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $message = "Starting";

        $output->writeln($message);
        $this->logger->info($message);
        $collection = $this->collection;
        $urlQueue = $this->amqpConnection->getQueue('urls');
        $feedQueue = $this->amqpConnection->getQueue('feedRegister');

        $urlQueue->consume(function($message) use ($collection, $feedQueue){
            $entity = $collection->findByProtocolRelativeUrl($message->body);
            if ( is_null($entity) ) {
                $this->logger->info("registering URL : {$message->body}");
                $url = new Url();
                $url->setValue($message->body);
                $url->setSubmissionDate(new \DateTime());
                $collection->persist($url);
                $feedQueue->enqueue($message->body);
            }

            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
        });

        $urlQueue->wait();
    }

}
