<?php declare(strict_types = 1);

namespace App\Command;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use App\Amqp\Connection as AmqpConnection;

class UrlScanCommand extends Command
{

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \App\Amqp\Connection
     */
    private $amqpConnection;

    /**
     *
     * @param LoggerInterface $logger
     * @param AmqpConnection $amqpConnection
     * @param Collection $collection
     */
    public function __construct(LoggerInterface $logger, AmqpConnection $amqpConnection)
    {
        $this->logger = $logger;
        $this->amqpConnection = $amqpConnection;

        parent::__construct();
    }

    protected function configure()
    {
        $this
        ->setName('pantagruel:url:scan')
        ->setDescription('Scan url')
        ->setHelp('This command scans a web page in search for new URLs');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $message = "Starting";

        $this->logger->info($message);
        $urlQueue = $this->amqpConnection->getQueue('urls');
        $urlScanQueue = $this->amqpConnection->getQueue('urlScan');

        $urlScanQueue->consume(function($message) use ($urlQueue){
            $this->logger->info("scanning URL : {$message->body}");

            try {
                $html = file_get_contents($message->body);
                $doc = new \DOMDocument();
                $doc->loadHTML($html, LIBXML_NOERROR);
                $xpath = new \DOMXpath($doc);
                $nodeList = $xpath->query('//a/@href');
                $urls = [];
                for ($i = 0; $i < $nodeList->length; $i++) {
                    $urls[] = $nodeList->item($i)->value;
                }
    
                $this->logger->debug("found urls : " . count($urls));
                $urls = array_unique($urls);
                $urlComponents = parse_url($message->body);
                $host = "{$urlComponents['scheme']}://{$urlComponents['host']}";
                $this->logger->debug("keeping urls from {$host} ");
                $finds = ['rss', 'atom', 'feed'];
                foreach($urls as $url) {
                    $match = ($url != str_replace($finds, '', $url));
                    $sameHost = (0 === strpos($url, $host));
                    if (filter_var($url, FILTER_VALIDATE_URL) && $sameHost && $match ) {
                        $this->logger->info("found : {$url}");
                        $urlQueue->enqueue($url);
                    }
                    $isFeedBurner = (false !== strpos($url, 'feedburner'));
                    if (filter_var($url, FILTER_VALIDATE_URL) && $isFeedBurner ) {
                        $this->logger->info("found feedburner : {$url}");
                        $urlQueue->enqueue($url);
                    }
                    
                }
            } catch (\Exception $e) {
                $this->logger->warning("failed to get {$message->body}");
                $this->logger->warning($e->getMessage());
                $this->logger->debug($e->getTraceAsString());
            }

            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
            sleep(1);
        });

        $urlScanQueue->wait();
    }

}
