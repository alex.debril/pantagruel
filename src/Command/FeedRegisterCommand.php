<?php declare(strict_types = 1);

namespace App\Command;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Amqp\Connection as AmqpConnection;
use App\Entity\Feed;
use App\Collection\Feed as Collection;
use FeedIo\FeedIo;

class FeedRegisterCommand extends Command
{

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \App\Amqp\Connection
     */
    private $amqpConnection;

    /**
     * @var \App\Collection\Feed
     */
    private $collection;

    /**
     *
     * @var \FeedIo\FeedIo
     */
    private $feedIo;

    /**
     * @param Session $session
     */
    public function __construct(
        LoggerInterface $logger, 
        FeedIo $feedIo, 
        AmqpConnection $ampqConnection,
        Collection $collection
        )
    {
        $this->logger = $logger;
        $this->feedIo = $feedIo;
        $this->amqpConnection = $ampqConnection;
        $this->collection = $collection;

        parent::__construct();
    }

    protected function configure()
    {
        $this
        ->setName('pantagruel:feed:register')
        ->setDescription('Register feeds')
        ->setHelp('This command stores feeds in database');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $message = "Starting";

        $this->logger->info($message);
        $collection = $this->collection;
        $feedIo = $this->feedIo;

        $feedRegisterQueue = $this->amqpConnection->getQueue('feedRegister');
        $feedDiscoverQueue = $this->amqpConnection->getQueue('feedDiscover');
        $urlScanQueue = $this->amqpConnection->getQueue('urlScan');

        $feedRegisterQueue->consume(function($message) use ($collection, $feedIo, $feedDiscoverQueue, $urlScanQueue){
            if ( is_null($collection->findByUrl($message->body)) ) {
                try {
                    $this->logger->info("registering feed : {$message->body}");
                    $result = $feedIo->read($message->body);

                    if ( $this->isAcceptable($result->getFeed()) ) {
                        $feed = new Feed();
                        $feed->setUrl($message->body);
                        $collection->persist($feed);
                        $this->logger->info("{$message->body} is actually a feed, queued for reading");
                    } else {
                        $this->logger->info("{$message->body} is a feed, but is not acceptable");
                    }

                } catch (\Exception $e) {
                    $this->logger->info("{$message->body} is not a feed, queued for feed discovery");
                    $feedDiscoverQueue->enqueue($message->body);
                    $urlScanQueue->enqueue($message->body);
                }
            }

            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
        });

        $feedRegisterQueue->wait();
    }

    protected function isAcceptable(\FeedIo\Feed $feed)
    {
        $nbItems = count($feed);
        $accepted = $nbItems > 1;

        $this->logger->info("Feed has {$nbItems} items. ");

        return $accepted;
    }

}
