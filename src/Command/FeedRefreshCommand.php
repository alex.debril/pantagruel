<?php declare(strict_types = 1);

namespace App\Command;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use App\Entity\Feed;
use App\Entity\Item;
use App\Collection\Feed as FeedCollection;
use App\Collection\Item as ItemCollection;
use App\Collection\Category as CategoryCollection;
use App\Collection\ReadAccess as ReadAccessCollection;
use FeedIo\FeedIo;

class FeedRefreshCommand extends Command
{

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \App\Collection\Feed
     */
    private $feedCollection;

    /**
     * @var \App\Collection\Item
     */
    private $itemCollection;

    /**
     * @var \App\Collection\Category
     */
    private $categoryCollection;

/**
     * @var \App\Collection\ReadAccess
     */
    private $readAccessCollection;

    /**
     *
     * @var \FeedIo\FeedIo
     */
    private $feedIo;

    /**
     *
     * @param LoggerInterface $logger
     * @param FeedIo $feedIo
     * @param FeedCollection $feedCollection
     * @param ItemCollection $itemCollection
     * @param CategoryCollection $categoryCollection
     * @param ReadAccessCollection $readAccessCollection
     */
    public function __construct(
        LoggerInterface $logger, 
        FeedIo $feedIo, 
        FeedCollection $feedCollection,
        ItemCollection $itemCollection,
        CategoryCollection $categoryCollection,
        ReadAccessCollection $readAccessCollection
        )
    {
        $this->logger = $logger;
        $this->feedIo = $feedIo;
        $this->feedCollection = $feedCollection;
        $this->itemCollection = $itemCollection;
        $this->categoryCollection = $categoryCollection;
        $this->readAccessCollection = $readAccessCollection;

        parent::__construct();
    }

    protected function configure()
    {
        $this
        ->setName('pantagruel:feed:refresh')
        ->setDescription('Refresh feeds')
        ->setHelp('This command refreshes feeds');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $message = "Starting";
        $this->logger->info($message);

        $this->wait($output);
    }

    protected function wait(OutputInterface $output)
    {
        $this->logger->info('scanning feeds to refresh');
        $feeds = $this->feedCollection->findModifiedBefore(new \DateTime('-120 minutes'));

        foreach($feeds as $feed) {
            $this->logger->info("will refresh {$feed->getTitle()}");
            $this->refresh($output, $feed);
        }

        $this->logger->info('waiting');
        sleep(120);
        $this->wait($output);
    }

    protected function refresh(OutputInterface $output, Feed $feed)
    {
        try {
            $this->logger->debug("{$feed->getUrl()} last modified before : {$feed->getLastModified()->format(\DateTime::ATOM)}");
            $readAccess = $this->readAccessCollection->newReadAccess($feed);
            $result = $this->feedIo->read($feed->getUrl(), $feed, $feed->getLastModified());
            $readAccess->record($result);
            $this->readAccessCollection->persist($readAccess);
            $this->feedCollection->persist($feed);

            $compDate = new \DateTime('@0');
            if ( $feed->getLastModified() == $compDate) {
                throw new \Exception("{$feed->getUrl()} : invalid last modified");
            }

            foreach($feed as $item) {
                $item->setFeed($feed);
                $this->storeCategories($item);
                $this->itemCollection->persist($item);
            }
        } catch (\Exception $e) {
            $this->logger->warning($e->getMessage());
        }

    }

    protected function storeCategories(Item $item)
    {
        $ids = [];
        foreach($item->getCategories() as $category) {
            if ( ! is_null($category->getLabel())) {
                $dbCategory = $this->categoryCollection->getByName($category->getLabel());
                $ids[] = $dbCategory->getId();
            }
        }

        $item->setCategoryIds($ids);
    }

}
