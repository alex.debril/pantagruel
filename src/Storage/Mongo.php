<?php declare(strict_types = 1);

namespace App\Storage;

use MongoDB\Client;

class Mongo
{

    /**
     * @var MongoDB\Client
     */
    private $client;

    /**
     * @var string
     */
    private $databaseName;

    /**
     * @var MongDB\Database
     */
    private $database;

    public function __construct(Client $client, string $databaseName)
    {
        $this->client = $client;
        $this->databaseName = $databaseName;
    }

    /**
     * [getDatabase description]
     * @return MongoDBDatabase [description]
     */
    public function getDatabase() : \MongoDB\Database
    {
        if ( is_null($this->database) ) {
            $this->database = $this->client->selectDatabase($this->databaseName);
        }

        return $this->database;
    }

}
