<?php declare(strict_types = 1);

namespace App\Amqp;

use Psr\Log\LoggerInterface;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class Connection
{

    /**
     * @var string
     */
    private $host;

    /**
     * @var integer
     */
    private $port;

    /**
     * @var string
     */
    private $user;

    /**
     * @var string
     */
    private $pass;

    /**
     * @var string
     */
    private $vhost;

    /**
     * @var \PhpAmqpLib\Connection\AMQPStreamConnection
     */
    private $streamConnection;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \ArrayIterator
     */
    private $queues;

    /**
     *
     * @param string $host
     * @param integer $port
     * @param string $user
     * @param string $pass
     * @param string $vhost
     */
    public function __construct(string $host, int $port, string $user, string $pass, string $vhost, LoggerInterface $logger)
    {
        $this->host = $host;
        $this->port = $port;
        $this->user = $user;
        $this->pass = $pass;
        $this->vhost = $vhost;
        $this->logger = $logger;
        $this->queues = new \ArrayIterator();
    }

    /**
     * @param string $name
     * @return Queue
     */
    public function getQueue(string $name) : Queue
    {
        if ( ! $this->queues->offsetExists($name) ) {
            $this->logger->info("Creating queue {$name}");
            $this->queues->offsetSet($name, new Queue($this->getStreamConnection(), $this->logger, $name));
        }

        return $this->queues->offsetGet($name);
    }

    /**
     *
     * @return AMQPStreamConnection
     */
    public function getStreamConnection() : AMQPStreamConnection
    {
        if (is_null($this->streamConnection)) {
            $this->streamConnection = $this->createStreamConnection();
        }

        return $this->streamConnection;
    }

    /**
     *
     * @param integer $retries
     * @param integer $delay
     * @return AMQPStreamConnection
     */
    public function createStreamConnection($retries = 10, $delay = 2) : AMQPStreamConnection
    {
        do {
            try {
                $this->logger->info("trying to connect to AMQP");
                $connection = new AMQPStreamConnection($this->host, $this->port, $this->user, $this->pass, $this->vhost);
                $this->logger->info("connection ok");

                return $connection;
            } catch (\Exception $e) {
                $this->logger->warning("connection failed, will retry in {$delay} seconds");
                $retries--;
                sleep($delay);
            }
        } while ($retries >= 0);

        throw new \RuntimeException("AMQP not available");
    }

}
