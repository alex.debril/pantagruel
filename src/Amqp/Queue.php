<?php declare(strict_types = 1);

namespace App\Amqp;

use Psr\Log\LoggerInterface;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Channel\AMQPChannel;

class Queue
{

    /**
     * Connection to the Queue system
     * @var \PhpAmqpLib\Connection\AMQPStreamConnection
     */
    private $connection;

    /**
     * Queue's channel
     * @var \PhpAmqpLib\Channel\AMQPChannel
     */
    private $channel;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $channelExchange;

    /**
     * @var string
     */
    private $channelQueue;

    /**
     *
     * @param AMQPStreamConnection $connection
     * @param string $name
     */
    public function __construct(AMQPStreamConnection $connection, LoggerInterface $logger, string $name)
    {
        $this->connection = $connection;
        $this->logger = $logger;
        $this->name = $name;
        $this->channelExchange = "exchange_{$name}";
        $this->channelQueue = "queue_{$name}";
    }

    /**
     *
     * @param mixed $payload
     * @return void
     */
    public function enqueue($payload)
    {
        if ( ! is_string($payload) ) {
            $payload = json_encode($payload);
        }

        $message = new AMQPMessage($payload, array('content_type' => 'text/plain', 'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT));
        $this->getChannel()->basic_publish($message, $this->channelExchange);
    }

    public function consume($callback)
    {
        $this->getChannel()->basic_consume($this->channelQueue, 'consumer', false, false, false, false, $callback);
    }

    /**
     * @return AMQPChannel
     */
    public function getChannel() : AMQPChannel
    {
        if ( is_null($this->channel) ) {
            $channel = $this->connection->channel();
            $channel->queue_declare($this->channelQueue, false, true, false, false);
            $channel->exchange_declare($this->channelExchange, 'direct', false, true, false);
            $channel->queue_bind($this->channelQueue, $this->channelExchange);

            $this->channel = $channel;
        }

        return $this->channel;
    }

    /**
     * 
     * 
     * @return void
     */
    public function wait()
    {
        $channel = $this->getChannel();

        $this->logger->info("queue {$this->name} waiting for messages");
        while (count($channel->callbacks)) {
            $channel->wait();
        }
    }
}
