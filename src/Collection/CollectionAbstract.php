<?php declare(strict_types = 1);

namespace App\Collection;

use App\Storage\Mongo;
use MongoDB\Collection;

abstract class CollectionAbstract
{

    const COLLECTION_NAME = 'abstract';

    protected $database;

    /**
     * [__construct description]
     * @param Mongo $mongo [description]
     */
    public function __construct(Mongo $mongo)
    {
        $this->database = $mongo->getDatabase();
    }

    public function getDatabase()
    {
        return $this->database;
    }

    /**
     *
     * @return integer
     */
    public function count() : int
    {
        return $this->getCollection()->count();
    }

    /**
     * @return MongoDBCollection [description]
     */
    public function getCollection() : \MongoDB\Collection
    {
        return $this->database->selectCollection(static::COLLECTION_NAME);
    }

}
