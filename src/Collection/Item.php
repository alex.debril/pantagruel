<?php declare(strict_types = 1);

namespace App\Collection;

use MongoDB\Collection;
use \MongoDB\BSON\UTCDatetime;
use \MongoDB\BSON\ObjectId;

use App\Entity\Item as Entity;


class Item extends CollectionAbstract
{

    const COLLECTION_NAME = 'item';

    public function countByFeedId(ObjectId $id)
    {
        return $this->getCollection()->count(['feedId' => ['$eq' => $id]]);
    }

    public function findByFeedId(ObjectId $id, int $limit = 25)
    {
        return $this->getCollection()->find(
            ['feedId' => ['$eq' => $id]],
            [
                'sort'  => ['lastModified' => 1],
                'limit' => $limit
                ]
        );
    }

    public function findRecordedAfter(\DateTime $date)
    {
        return $this->getCollection()->find(
            ['recordDate' => ['$gt' => new UTCDatetime($date->getTimestamp()*1000)]],
            ['sort' => ['recordDate' => 1]]
        );
    }

    public function persist(Entity $entity)
    {
        $this->getCollection()->insertOne($entity);
    }

}
