<?php declare(strict_types = 1);

namespace App\Collection;

use MongoDB\BSON\UTCDatetime;

use App\Entity\Feed as Entity;


class Feed extends CollectionAbstract
{

    const COLLECTION_NAME = 'feed';

    public function findAll()
    {
        return $this->getCollection()->find();
    }

    public function listUrls(int $start, int $num = 100)
    {
        return $this->getCollection()->find([], ['skip' => $start, 'limit' => $num]);
    }


    public function findByFunctionnalId(string $value)
    {
        return $this->getCollection()->findOne(['functionnalId' => ['$eq' => $value]]);
    }

    public function findByUrl(string $value)
    {
        return $this->getCollection()->findOne(['url' => ['$eq' => $value]]);
    }

    public function findModifiedBefore(\DateTime $date)
    {
        return $this->getCollection()->find(
            ['lastModified' => ['$lt' => new UTCDatetime($date->getTimestamp()*1000)]],
            ['sort' => ['lastModified' => 1]]
        );
    }

    public function persist(Entity $entity)
    {
        if ( is_null($entity->getId()) ) {
            $this->getCollection()->insertOne($entity);
        } else {
            $this->getCollection()->updateOne(
                ['_id' => $entity->getId()], 
                ['$set' => $entity]
            );
        }
        
    }

}
