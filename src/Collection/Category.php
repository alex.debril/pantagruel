<?php declare(strict_types = 1);

namespace App\Collection;

use MongoDB\BSON\UTCDatetime;
use App\Entity\Category as Entity;


class Category extends CollectionAbstract
{

    const COLLECTION_NAME = 'category';

    public function getByName(string $value) : Entity
    {
        $category = $this->findByName($value);

        if ( is_null($category) ) {
            $category = new Entity;
            $category->setName($value);
            $this->persist($category);
            $category = $this->findByName($value);
        }

        return $category;
    }

    public function findByName(string $value)
    {
        return $this->getCollection()->findOne(['name' => ['$eq' => $value]]);
    }

    public function persist(Entity $entity)
    {
        if ( is_null($entity->getId()) ) {
            $this->getCollection()->insertOne($entity);
        } else {
            $this->getCollection()->updateOne(
                ['_id' => $entity->getId()], 
                ['$set' => $entity]
            );
        }
        
    }

}
