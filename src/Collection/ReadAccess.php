<?php declare(strict_types = 1);

namespace App\Collection;

use App\Entity\ReadAccess as Entity;
use App\Entity\Feed;


class ReadAccess extends CollectionAbstract
{

    const COLLECTION_NAME = 'read_access';

    /**
     *
     * @param Feed $feed
     * @return void
     */
    public function newReadAccess(Feed $feed)
    {
        return new Entity($feed);
    }

    public function persist(Entity $entity)
    {
        $this->getCollection()->insertOne($entity);
    }

}
