<?php declare(strict_types = 1);

namespace App\Collection;

use App\Entity\Url as Entity;


class Url extends CollectionAbstract
{

    const COLLECTION_NAME = 'url';

    public function findByProtocolRelativeUrl(string $value)
    {
        return $this->getCollection()->findOne(
            ['protocolRelativeUrl' => ['$eq' => str_replace(['http:', 'https:', 'www.'], '', $value)]]
        );
    }

    public function persist(Entity $entity)
    {
        $this->getCollection()->insertOne($entity);
    }

}
