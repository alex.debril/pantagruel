<?php declare(strict_types = 1);

namespace App\Entity;

use \MongoDB\BSON\Persistable;
use \MongoDB\BSON\UTCDatetime;
use \MongoDB\BSON\ObjectId;

class Category implements Persistable
{

    private $id;

    private $name;

    /**
     * @return ObjectId|null
     */
    public function getId() : ? ObjectId
    {
        return $this->id;
    }

    /**
     * @param ObjectId $id
     * @return void
     */
    public function setId(ObjectId $id) : \App\Entity\Category
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     *
     * @param string $value
     * @return \App\Entity\Category
     */
    public function setName(string $name) : \App\Entity\Category
    {
        $this->name = $name;

        return $this;
    }

    public function bsonSerialize()
    {
        return [
            'name' => $this->getName()
        ];
    }

    public function bsonUnserialize(array $data)
    {
        $this->setId($data['_id']);
        $this->setName($data['name']);
    }

}
