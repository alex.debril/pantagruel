<?php declare(strict_types = 1);

namespace App\Entity;

use \MongoDB\BSON\Persistable;
use \MongoDB\BSON\UTCDatetime;

class Url implements Persistable
{
    private $value;

    private $protocolRelativeUrl;

    private $submissionDate;

    /**
     * @return string [description]
     */
    public function getValue() : string
    {
        return $this->value;
    }

    /**
     *
     * @return string
     */
    public function getProtocolRelativeUrl() : string
    {
        return $this->protocolRelativeUrl;
    }

    /**
     * @param  string          $url [description]
     * @return AppEntityUrl      [description]
     */
    public function setValue(string $value) : \App\Entity\Url
    {
        $this->value = $value;
        $this->protocolRelativeUrl = str_replace(['http:', 'https:', 'www.'], '', $value);

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getSubmissionDate() : ? \DateTime
    {
        return $this->submissionDate;
    }

    /**
     * @param \DateTime $date
     * @return void
     */
    public function setSubmissionDate(\DateTime $date) : \App\Entity\Url
    {
        $this->submissionDate = $date;

        return $this;
    }

    public function bsonSerialize()
    {
        return [
            'value' => $this->getValue(),
            'protocolRelativeUrl' => $this->getProtocolRelativeUrl(),
            'submissionDate' => new UTCDatetime($this->getSubmissionDate()->getTimestamp()*1000)
        ];
    }

    public function bsonUnserialize(array $data)
    {
        $this->setValue($data['value']);
        $this->setSubmissionDate($data['submissionDate']->toDateTime());
    }

}
