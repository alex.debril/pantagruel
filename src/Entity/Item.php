<?php declare(strict_types = 1);

namespace App\Entity;

use \MongoDB\BSON\Persistable;
use \MongoDB\BSON\UTCDatetime;

use FeedIo\Feed\Item as FeedIoItem;

class Item extends FeedIoItem implements Persistable
{

    /**
     * @var \App\Entity\Feed
     */
    private $feed;

    /**
     * @var \DateTime
     */
    private $recordDate;

    /**
     * @var array
     */
    private $categoryIds = [];

    public function __construct()
    {
        $this->setRecordDate(new \DateTime());

        parent::__construct();
    }

    /**
     * @return Feed
     */
    public function getFeed(): Feed
    {
        return $this->feed;
    }

    public function setFeed(Feed $feed)
    {
        $this->feed = $feed;
    }

    public function getRecordDate() : \DateTime
    {
        return $this->recordDate;
    }

    /**
     *
     * @param \DateTime $recordDate
     * @return void
     */
    public function setRecordDate(\DateTime $recordDate)
    {
        $this->recordDate = $recordDate;

        return $this;
    }

    /**
     * @return array
     */
    public function getCategoryIds() : array
    {
        return $this->categoryIds;
    }

    /**
     * @param array $ids
     */
    public function setCategoryIds(array $ids)
    {
        $this->categoryIds = $ids;

        return $this;
    }

    public function bsonSerialize()
    {
        $lastModified = $this->getLastModified() ?? new \DateTime('@0');

        return [
            'feedId' => $this->getFeed()->getId(),
            'link' => $this->getLink(),
            'title' => $this->getTitle(),
            'author' => $this->getAuthor(),
            'recordDate' =>  new UTCDatetime($this->recordDate->getTimestamp()*1000),
            'lastModified' =>  new UTCDatetime($lastModified->getTimestamp()*1000),
            'publicId' => $this->getPublicId(),
            'description' => $this->getDescription(),
            'categoryIds' => $this->getCategoryIds(),
            /*'elements' => iterator_to_array($this->getElementsGenerator()),
            'categories' => $this->getCategories(),
            'medias' => $this->getMedias(),*/
        ];
    }

    public function bsonUnserialize(array $data)
    {
        $this->setLink($data['link']);
        $this->setDescription($data['description']);
        $this->setTitle($data['title']);
        $this->setLastModified($data['lastModified']->toDateTime());
    }

}