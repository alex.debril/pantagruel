<?php declare(strict_types = 1);

namespace App\Entity;

use \MongoDB\BSON\Persistable;
use \MongoDB\BSON\UTCDatetime;
use \MongoDB\BSON\ObjectId;

use FeedIo\Feed as FeedIoFeed;
use FeedIo\Feed\ItemInterface;

class Feed extends FeedIoFeed implements Persistable
{

    private $id;

    private $functionnalId;

    /**
     * @return string
     */
    public function getFunctionnalId() : string
    {
        if ( is_null($this->functionnalId)) {
            $this->functionnalId = $this->slugify($this->getUrl());
        }

        return $this->functionnalId;
    }

    public function setFunctionnalId(string $id)
    {
        $this->functionnalId = $id;

        return $this;
    }

    /**
     * @return ObjectId|null
     */
    public function getId() : ? ObjectId
    {
        return $this->id;
    }

    /**
     * @param ObjectId $id
     * @return void
     */
    public function setId(ObjectId $id) : \App\Entity\Feed
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return ItemInterface
     */
    public function newItem() : ItemInterface
    {
        return new Item;
    }

    public function bsonSerialize()
    {
        $lastModified = $this->getLastModified() ?? new \DateTime('@0');

        return [
            'url' => $this->getUrl(),
            'functionnalId' => $this->getFunctionnalId(),
            'description' => $this->getDescription(),
            'lastModified' => new UTCDatetime($lastModified->getTimestamp()*1000),
            'link' => $this->getLink(),
            'title' => $this->getTitle(),
            'publicId' => $this->getPublicId(),
            'elements' => $this->getAllElements(),
            'categories' => $this->getCategories(),
        ];
    }

    public function bsonUnserialize(array $data)
    {
        $this->setId($data['_id']);
        $this->setUrl($data['url']);
        $this->setDescription($data['description']);
        $this->setLastModified($data['lastModified']->toDateTime());
        $this->setLink($data['link']);
        $this->setTitle($data['title']);
        $this->setPublicId($data['publicId']);

        if (isset($data['functionnalId'])) {
            $this->setFunctionnalId($data['functionnalId']);
        }

        parent::__construct();

        return $this;
    }

    protected function slugify(string $string): string
    {
        $string = str_replace(["'", '"', ':', '?', 'https', 'http', '//'], '', $string);
        $string = str_replace(['.', '/', '--'], '-', $string);
        $string = trim($string, '-');
        return preg_replace('/\s+/', '-', mb_strtolower(trim(strip_tags($string)), 'UTF-8'));
    }

}
