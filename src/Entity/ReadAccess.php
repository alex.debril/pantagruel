<?php declare(strict_types = 1);

namespace App\Entity;

use \FeedIo\Reader\Result;
use \MongoDB\BSON\Persistable;
use \MongoDB\BSON\UTCDatetime;

class ReadAccess implements Persistable
{

    /**
     * @var \App\Entity\Feed
     */
    private $feed;

    private $startTime;

    private $duration = 0;

    private $httpHeaders;

    private $numItems;

    private $url;

    private $modifiedSince;

    /**
     * @param Feed $feed
     */
    public function __construct(Feed $feed)
    {
        $this->startTime = microtime(true);
        $this->feed = $feed;
    }

    public function record(Result $result)
    {
        $this->accessDate = new \DateTime();
        $this->duration = microtime(true) - $this->startTime;
        $this->httpHeaders = $result->getResponse()->getHeaders();
        $this->numItems = count($result->getFeed());
        $this->url = $result->getUrl();
        $this->modifiedSince = $result->getModifiedSince();

        return $this;
    }

    public function bsonSerialize()
    {
        return [
            'accessDate' => new UTCDatetime($this->accessDate->getTimestamp()*1000),
            'feedId' => $this->feed->getId(),
            'url' => $this->url,
            'modifiedSinceSent' => new UTCDatetime($this->modifiedSince->getTimestamp()*1000),
            'duration' => $this->duration,
            'numItems' => $this->numItems,
            'httpHeaders' => $this->httpHeaders,
        ];
    }

    public function bsonUnserialize(array $data)
    {

    }

}
