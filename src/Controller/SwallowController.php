<?php

namespace App\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

use App\Amqp\Connection as AmqpConnection;

class SwallowController
{

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \App\Amqp\Connection
     */
    private $amqpConnection;

    /**
     *
     * @param LoggerInterface $logger
     * @param AmqpConnection $amqpConnection
     */
    public function __construct(LoggerInterface $logger, AmqpConnection $amqpConnection)
    {
        $this->logger = $logger;
        $this->amqpConnection = $amqpConnection;
    }

    /**
     * @Route("/swallow", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function action(Request $request) : JsonResponse
    {
        try {
            $data = json_decode($request->getContent(), true);

            $enqueued = [];
            $queue = $this->amqpConnection->getQueue('urls');
            foreach($data['urls'] as $url) {
                if( filter_var($url, FILTER_VALIDATE_URL)) {
                    $this->logger->info("enqueue {$url}");
                    $queue->enqueue($url);
                    $enqueued[] = $url;
                }
            }

            return new JsonResponse(['enqueued' => $enqueued]);
        } catch (\Exception $e) {
            return new JsonResponse(
                [
                    'type' => get_class($e),
                    'title' => $e->getMessage(),
                ],
                JsonResponse::HTTP_INTERNAL_SERVER_ERROR,
                [
                    'Content-Type' => 'application/problem+json'
                ]
            );
        }
    }    
}
