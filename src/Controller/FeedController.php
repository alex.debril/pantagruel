<?php

namespace App\Controller;

use App\Collection\Feed as FeedCollection;
use App\Collection\Item as ItemCollection;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Item;
use App\Entity\Feed;

class FeedController
{

    /**
     * @var \App\Collection\Feed
     */
    private $feedCollection;

    /**
     * @var \App\Collection\Item
     */
    private $itemCollection;

    /**
     *
     * @param FeedCollection $feedCollection
     */
    public function __construct(FeedCollection $feedCollection, ItemCollection $itemCollection)
    {
        $this->feedCollection = $feedCollection;
        $this->itemCollection = $itemCollection;
    }

    /**
     * @Route("/feeds/list/{offset}/{limit}", defaults={"offset"=0, "limit"=100})
     *
     * @return void
     */
    public function listFeeds(int $offset = 0, int $limit = 100)
    {
        $list = $this->feedCollection->listUrls($offset * $limit, $limit);
        $count = $this->feedCollection->count();
        $pages = ceil($count / $limit);

        $feeds = [];

        foreach($list as $feed) {
            $feeds[] = $this->feedToArray($feed);
        }

        return new JsonResponse(
            [
                'cursor' => [
                    'offset' => $offset,
                    'limit' => $limit,
                    'pages' => $pages,
                ],
                'count' => $count,
                'feeds' => $feeds
            ]);
    }

    /**
     * @Route("/feed/{id}/{limit}", defaults={"limit"=25})
     *
     * @param string $id
     * @return void
     */
    public function getFeed(string $id, int $limit = 25)
    {
        $feed = $this->feedCollection->findByFunctionnalId($id);
        $items = $this->itemCollection->findByFeedId($feed->getId(), $limit);

        $data = [
            'feed' => $this->feedToArray($feed),
            'numItems' => $this->itemCollection->countByFeedId($feed->getId())
        ];

        $data['items'] = [];
        foreach($items as $item) {
            $data['items'][] = $this->itemToArray($item);
        }

        return new JsonResponse($data);
    }

    protected function feedToArray(Feed $feed) : array
    {
        $created = new \DateTime();
        $created->setTimestamp($feed->getId()->getTimestamp());

        return [
            'url' => $feed->getUrl(),
            'id' => $feed->getFunctionnalId(),
            'title' => $feed->getTitle(),
            'created' => $created->format(\DateTime::ATOM),
            'lastModified' => $feed->getLastModified()->format(\DateTime::ATOM),
        ];
    }

    protected function itemToArray(Item $item) : array
    {
        return [
            'title' => $item->getTitle(),
            'description' => $item->getDescription(),
            'lastModified' => $item->getLastModified()->format(\DateTime::ATOM),
        ];
    }

}
