<?php

namespace App\Controller;

use App\Collection\Category as CategoryCollection;
use App\Collection\Feed as FeedCollection;
use App\Collection\Item as ItemCollection;
use App\Collection\ReadAccess as ReadAccessCollection;
use App\Collection\Url as UrlCollection;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

class StatsController
{

    /**
     * @var \App\Collection\Category
     */
    private $categoryCollection;

    /**
     * @var \App\Collection\Feed
     */
    private $feedCollection;

    /**
     * @var \App\Collection\Item
     */
    private $itemCollection;

    /**
     * @var \App\Collection\Url
     */
    private $urlCollection;

    /**
     * @var \App\Collection\ReadAccess
     */
    private $readAccessCollection;

    /**
     *
     * @param CategoryCollection $categoryCollection
     * @param FeedCollection $feedCollection
     * @param ItemCollection $itemCollection
     * @param ReadAccessCollection $readAccessCollection
     * @param UrlCollection $urlCollection
     */
    public function __construct(
        CategoryCollection $categoryCollection,
        FeedCollection $feedCollection,
        ItemCollection $itemCollection,
        ReadAccessCollection $readAccessCollection,
        UrlCollection $urlCollection
        )
    {
        $this->categoryCollection = $categoryCollection;
        $this->feedCollection = $feedCollection;
        $this->itemCollection = $itemCollection;
        $this->readAccessCollection = $readAccessCollection;
        $this->urlCollection = $urlCollection;
    }

    /**
     * @Route("/stats")
     * 
     * @param Request $request
     * @return JsonResponse
     */
    public function getGlobal(Request $request) : JsonResponse
    {
        $database = $this->feedCollection->getDatabase();
        $stats = iterator_to_array($database->command(['dbStats' => 1]));

        
        $stats[0]['Data Size'] = $this->getSize($stats[0]['dataSize']);
        $stats[0]['Storage Size'] = $this->getSize($stats[0]['storageSize']);
        $stats[0]['Index Size'] = $this->getSize($stats[0]['indexSize']);

        return new JsonResponse([
            'categories' => $this->categoryCollection->count(),
            'feeds' => $this->feedCollection->count(),
            'items' => $this->itemCollection->count(),
            'read_accesses' => $this->readAccessCollection->count(),
            'url' => $this->urlCollection->count(),
            'database' => $stats
            ]);
    }

    protected function getSize(int $size)
    {
        $units = [
            3 => 'octets',
            6 => 'Ko',
            9 => 'Mo',
            12 => 'Go',
        ];

        $i = 0;
        foreach($units as $pow => $unit) {
            $operand = max(1, 1024**$i);
            $literal = round($size/$operand, 2);
            if ($size < 1 * 10 ** $pow) {
                return $literal . $unit;
            }
            $i++;
        }
    }

}
